package pe.uni.jesusramirezm.pc4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    TextView textViewPlate;
    Spinner spinnerLogo;
    EditText editTextName, editTextAddress;
    Button button;
    ArrayAdapter<CharSequence> adapter;
    LinearLayout linearLayout;
    RadioGroup radioGroup;
    RadioButton radioButton1;
    RadioButton radioButton2;
    String metodoPago;
    int cantidadPlatos;

    SharedPreferences sharedPreferences;
    String nameSave ;
    String addressSave;
    boolean isChecked1Save;
    boolean isChecked2Save;
    int cantidadPlatosSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = findViewById(R.id.edit_text_name);
        editTextAddress = findViewById(R.id.edit_text_address);
        button = findViewById(R.id.button);
        textViewPlate = findViewById(R.id.text_view_plate);
        spinnerLogo = findViewById(R.id.spinner);
        linearLayout = findViewById(R.id.linearLayout);
        radioGroup = findViewById(R.id.radio_group);
        radioButton1 = findViewById(R.id.radio_button_logo_1);
        radioButton2 = findViewById(R.id.radio_button_logo_2);

        // Obtenemos la instancia que se realizó. Con ello también los datos
        Intent intent = getIntent();
        String text = intent.getStringExtra("TEXT");

        // Para concatenar cadenas en el XML
        Resources res = getResources();

        textViewPlate.setText(String.format(res.getString(R.string.plate), text));

        adapter = ArrayAdapter.createFromResource(this, R.array.logos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerLogo.setAdapter(adapter);
        spinnerLogo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {

                    case 0: cantidadPlatos = 1; break;
                    case 1: cantidadPlatos = 2; break;
                    case 2: cantidadPlatos = 3; break;
                    case 3: cantidadPlatos = 4;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Al hacer click
        button.setOnClickListener(v -> {

            // Obtener texto escrito en el editText
            String name = editTextName.getText().toString();
            String address = editTextAddress.getText().toString();

            // Si algún campo está vacio o hay errores con los radioButton
            if (name.equals("") || address.equals("") || (!radioButton1.isChecked() && !radioButton2.isChecked()) || (radioButton1.isChecked() && radioButton2.isChecked())) {

                Snackbar.make(linearLayout, R.string.msg_snack_bar, Snackbar.LENGTH_INDEFINITE).setAction("Cerrar", v1 -> {

                }).show();
            }

            // Si está lleno
            else {

                if (radioButton1.isChecked()) {
                    metodoPago = "Visa";
                }

                if (radioButton2.isChecked()) {
                    metodoPago = "Efectivo";
                }

                // Tipo de alerta Builder
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("");
                builder.setCancelable(false);
                // Mensaje que mostraremos, el cual cuenta con 3 variables en el XML
                builder.setMessage(String.format(res.getString(R.string.order_ready), name, address, metodoPago));

                // Respuesta positiva
                builder.setPositiveButton("Sí", (dialog, which) -> {
                    // Proceso para cerrar la aplicación
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                // Respuesta negativa
                builder.setNegativeButton("No", (dialog, which) -> dialog.cancel());

                builder.create().show();


            }

        });

        retrieveData();

    }

    // FUNCIÓN POR DONDE LA APLICACIÓN SIEMPRE PASA
    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    // Ahora veremos como guardar estos datos, incluso si cerramos la aplicación.
    private void saveData() {

        // Instancia
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        // Datos a guardar
        nameSave = editTextName.getText().toString();
        addressSave = editTextAddress.getText().toString();
        isChecked1Save = radioButton1.isChecked();
        isChecked2Save = radioButton2.isChecked();
        cantidadPlatosSave = cantidadPlatos;

        SharedPreferences.Editor editor = sharedPreferences.edit();

        // Llaves asociadas con las que recuperaremos los datos
        editor.putString("key name", nameSave);
        editor.putString("key message", addressSave);
        editor.putBoolean("key remember_1", isChecked1Save);
        editor.putBoolean("key remember_2", isChecked1Save);
        editor.putInt("key quantity", cantidadPlatosSave);
        editor.apply();

        Toast.makeText(getApplicationContext(), "Tus datos están guardados", Toast.LENGTH_LONG)
                .show();

    }

    // Para recuperar los datos guardados
    private void retrieveData() {

        // Instancia
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        // Recuperando datos con las llaves
        nameSave = sharedPreferences.getString("key name", null);
        addressSave = sharedPreferences.getString("key message", null);
        isChecked1Save = sharedPreferences.getBoolean("key remember_1", false);
        isChecked2Save = sharedPreferences.getBoolean("key remember_2", false);
        cantidadPlatosSave = sharedPreferences.getInt("key quantity", 1);

        // Asignamos los datos a los componentes
        editTextName.setText(nameSave);
        editTextAddress.setText(addressSave);
        radioButton1.setChecked(isChecked1Save);
        radioButton2.setChecked(isChecked2Save);
        spinnerLogo.setSelection(cantidadPlatosSave-1);
    }
}