package pe.uni.jesusramirezm.pc4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> text;
    ArrayList<Integer> image;
    ArrayList<String> description;

    // Constructor
    public GridAdapter(Context context, ArrayList<String> text, ArrayList<Integer> image, ArrayList<String> description) {

        this.context = context;
        this.text = text;
        this.image = image;
        this.description = description;
    }

    @Override
    public int getCount() {
        // Tamaño del arreglo de textos
        return text.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Para obtener las vistas
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_layout, parent, false);
        ImageView imageView = view.findViewById(R.id.image_view_character);
        TextView textView = view.findViewById(R.id.text_view_character);
        TextView textViewDescription = view.findViewById(R.id.text_view_description);

        imageView.setImageResource(image.get(position));
        textView.setText(text.get(position));
        textViewDescription.setText(description.get(position));

        return view;
    }
}
