package pe.uni.jesusramirezm.pc4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class FirstActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();
    ArrayList<String> description = new ArrayList<>();

    String platoEscogido = "";
    String platoEscogidoSave;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        retrieveData();

        /* Si platoEscogido es diferente de una cadena vacía; es decir, si hemos recuperado
        un plato, entonces pasamos a la segunda actividad automáticamente */
        if (!platoEscogido.equals("")) {
            // Segunda actividad
            Intent intent = new Intent( FirstActivity.this, MainActivity.class);

            // Le damos el dato a la segunda actividad
            intent.putExtra("TEXT", platoEscogido);

            startActivity(intent);
            finish();
        }

        gridView = findViewById(R.id.grid_view);
        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this, text, image, description);
        gridView.setAdapter(gridAdapter);

        // Al hacer click
        gridView.setOnItemClickListener((parent, view, position, id) -> {

            Toast.makeText(getApplicationContext(), text.get(position), Toast.LENGTH_LONG)
                    .show();

            platoEscogido = text.get(position);

            // Segunda actividad
            Intent intent = new Intent( FirstActivity.this, MainActivity.class);

            // Le damos el dato a la segunda actividad
            intent.putExtra("TEXT", platoEscogido);

            startActivity(intent);
            finish();
        });

    }

    // Función para llenar el GridView con las imágenes
    private void fillArray() {

        text.add("Cheesecake");
        text.add("Ensalada");
        text.add("Flan");
        text.add("Gelatina");
        text.add("Hamburguesa");
        text.add("Helado");
        text.add("Plátano");
        text.add("Pollo");

        description.add("Un postre");
        description.add("Una entrada");
        description.add("Un postre");
        description.add("Un postre");
        description.add("Comida rápida");
        description.add("Un postre");
        description.add("Una fruta");
        description.add("Un segundo");

        image.add(R.drawable.cheesecake);
        image.add(R.drawable.ensalada);
        image.add(R.drawable.flan);
        image.add(R.drawable.gelatina);
        image.add(R.drawable.hamburguesa);
        image.add(R.drawable.helado);
        image.add(R.drawable.platano);
        image.add(R.drawable.pollo);

    }

    // FUNCIÓN POR DONDE LA APLICACIÓN SIEMPRE PASA
    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    // Ahora veremos como guardar estos datos, incluso si cerramos la aplicación.
    private void saveData() {

        // Instancia
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        // Datos a guardar
        platoEscogidoSave = platoEscogido;

        SharedPreferences.Editor editor = sharedPreferences.edit();

        // Llaves asociadas con las que recuperaremos los datos
        editor.putString("key plate", platoEscogidoSave);
        editor.apply();


    }

    // Para recuperar los datos guardados
    private void retrieveData() {

        // Instancia
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        // Recuperando datos con las llaves
        platoEscogidoSave = sharedPreferences.getString("key plate", null);

        // Asignamos los datos a los componentes
        platoEscogido = platoEscogidoSave;

        /* NOTA PARA MÍ, IGNORAR: PARA COMPROBAR EL FUNCIONAMIENTO DE LOS DATOS GUARDADOS,
        DESCOMENTA LA LINEA SIGUIENTE Y EJECUTA LA APLICACIÓN. SIN ELEGIR UN PLATO, CIÉRRALA,
        COMENTA LA LÍNEA Y EJECUTA.
         */
        //platoEscogido = "";

    }
}